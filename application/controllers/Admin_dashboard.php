<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin_dashboard extends CI_Controller {
	public function index()
	{	
		if(isset($_SESSION['user_id']))
		{
		
		$this->load->helper('form', 'date');
        $this->load->model('usr_posts', 'articles');
		
		$config = [
		    "base_url" => base_url('admin_dashboard/index'),
			"per_page" => 10,
			"total_rows" => $this->articles->cunt_list(),
			"full_tag_open" => "<ul class='pagination justify-content-center mb-4'>",
			"full_tag_close" => "</ul>",
			"first_tag_open" => "<li>",
			"first_tag_close" => "</li>",
			"first_link" => "Prev",
			"last_link" => "Next",
			"last_tag_open" => "<li>",
			"last_tag_close" => "</li>",
			"next_tag_open" => "<li>",
			"next_tag_close" => "</li>",
			"prev_tag_open" => "<li>",
			"prev_tag_close" => "</li>",
			"num_tag_open" => "<li>",
			"num_tag_close" => "</li>",
			"cur_tag_open" => "<li class='active'><a>",
			"cur_tag_close" => "</a></li>"			
		   ];
	        $this->pagination->initialize($config);		
        $articles = $this->articles->articles_list($config['per_page'], $this->uri->segment(3));
        $this->load->view('admin/dashboard', array('articles' => $articles));
	    }
	    else{
	    	redirect('/user_login');
	    }
	}

	public function post_add()
	{
		$file_up = array(
                'upload_path' => 'assets/images',
                'allowed_types' => 'gif|jpg|png|jpeg'
         	 );

		$this->load->library('upload', $file_up);

		$this->load->library('form_validation');

		$this->form_validation->set_rules('post_title', 'Title', 'required');

		$this->form_validation->set_rules('post_desc', 'Post Description', 'required');

        $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");

        if ($this->form_validation->run() && $this->upload->do_upload('post_img'))

         {

         	$up_data = $this->upload->data();

         	$now = date('d-M-Y');

         	$postdata = array(

         		'user_id' => $this->session->userdata('user_id'),

         		'title' => $this->input->post('post_title'),

         		'image_url' => base_url('assets/images/'.$up_data['raw_name'].$up_data['file_ext']),

         		'msg_body' => $this->input->post('post_desc'),

         		'post_date' => $now         		

         	);

         	$this->load->model('add_post');

                	if(!$this->add_post->add_post_data($postdata)){	

                        return redirect('admin_dashboard');

                    }

         }

         else{

         	$upload_error = $this->upload->display_errors();

         	$this->load->view('admin/dashboard', compact('upload_error'));

         }

	}

		
	public function post_upd(){
		if(isset($_POST['usr_can'])){
			return redirect('admin_dashboard');
			}
		if(isset($_POST['upd_post'])){
		$file_up = array(
                'upload_path' => 'assets/images',
                'allowed_types' => 'gif|jpg|png|jpeg'
         	 );

		$this->load->library('upload', $file_up);

		$this->load->library('form_validation');

		$this->form_validation->set_rules('post_title', 'Title', 'required');

		$this->form_validation->set_rules('post_desc', 'Post Description', 'required');

        $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");

        if ($this->form_validation->run() && $this->upload->do_upload('post_img'))

         {

         	$up_data = $this->upload->data();

         	$now = date('d-M-Y');

         	$updtdata = array(
			   'id' => $this->input->post('post_id'),

         		'user_id' => $this->session->userdata('user_id'),

         		'title' => $this->input->post('post_title'),

         		'image_url' => base_url('assets/images/'.$up_data['raw_name'].$up_data['file_ext']),

         		'msg_body' => $this->input->post('post_desc'),

         		'post_date' => $now         		

         	);

         	$this->load->model('usr_posts');

                	if(!$this->usr_posts->edt_post($updtdata)){	

                        return redirect('admin_dashboard');

                    }
         }

         else{

         	$upload_error = $this->upload->display_errors();

         	$this->load->view('admin/dashboard', compact('upload_error'));

          }
		}
	}
		
	public function post_dlte(){
		$id = $_GET['id'];
		$this->load->model('usr_posts');
	    $this->usr_posts->delete_post($id);
	    redirect('admin_dashboard');
		
		}

}