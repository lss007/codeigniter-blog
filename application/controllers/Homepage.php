<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Homepage extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/home

	 *	- or -

	 * 		http://example.com/index.php/home/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	public function index()

	{
	       $this->load->model('all_posts', 'allposts');
	       $config = [
		    "base_url" => base_url('homepage/index'),
			"per_page" => 3,
			"total_rows" => $this->allposts->num_rows(),
			"full_tag_open" => "<ul class='pagination justify-content-center mb-4'>",
			"full_tag_close" => "</ul>",
			"first_tag_open" => "<li>",
			"first_tag_close" => "</li>",
			"first_link" => "Prev",
			"last_link" => "Next",
			"last_tag_open" => "<li>",
			"last_tag_close" => "</li>",
			"next_tag_open" => "<li>",
			"next_tag_close" => "</li>",
			"prev_tag_open" => "<li>",
			"prev_tag_close" => "</li>",
			"num_tag_open" => "<li>",
			"num_tag_close" => "</li>",
			"cur_tag_open" => "<li class='active'><a>",
			"cur_tag_close" => "</a></li>"			
		   ];
	        $this->pagination->initialize($config);
                $allposts = $this->allposts->posts_data($config['per_page'], $this->uri->segment(3));                         
		$this->load->view('home', ['allposts' => $allposts]);

	}

}
