<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Logout extends CI_Controller
{
	public function index()
	{
		if(isset($_SESSION['user_id']))
		{
		unset($_SESSION['user_id']);
		return redirect('homepage');
	    }
	}
}