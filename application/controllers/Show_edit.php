<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Show_edit extends CI_Controller {
	public function index()
	{	
		if(isset($_SESSION['user_id']))
		{
		
		$this->load->helper('form', 'date');
        $this->load->model('usr_posts', 'articles');
		
		$config = [
		    "base_url" => base_url('admin_dashboard/index'),
			"per_page" => 10,
			"total_rows" => $this->articles->cunt_list(),
			"full_tag_open" => "<ul class='pagination justify-content-center mb-4'>",
			"full_tag_close" => "</ul>",
			"first_tag_open" => "<li>",
			"first_tag_close" => "</li>",
			"first_link" => "Prev",
			"last_link" => "Next",
			"last_tag_open" => "<li>",
			"last_tag_close" => "</li>",
			"next_tag_open" => "<li>",
			"next_tag_close" => "</li>",
			"prev_tag_open" => "<li>",
			"prev_tag_close" => "</li>",
			"num_tag_open" => "<li>",
			"num_tag_close" => "</li>",
			"cur_tag_open" => "<li class='active'><a>",
			"cur_tag_close" => "</a></li>"			
		   ];
	        $this->pagination->initialize($config);		
        $articles = $this->articles->articles_list($config['per_page'], $this->uri->segment(3));
				
		if(isset($_GET['id'])){
		$id = $_GET['id'];
	    $dataarticles = $this->articles->show_edt_post($id);
		$this->load->view('admin/showpost', array('articles' => $articles, 'dataarticles' => $dataarticles));
		}
		else{
			$this->load->view('admin/dashboard', array('articles' => $articles));
			}
	    }
	    else{
	    	redirect('/user_login');
	    }
	}
}
