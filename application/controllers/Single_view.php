<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Single_view extends CI_Controller {
	public function index()	{
            $id = $_GET['id'];		
	    $this->load->model('single_data');
	    $sig_data = $this->single_data->sing_pst($id);
	    $this->load->view('single_post', array('sig_data' => $sig_data));		
	}
	
}
