<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User_login extends CI_Controller {
	public function index()
	{
		$this->load->helper('form', 'security');
		$this->load->view('login');
	}

	public function usr_log(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('pass', 'Password', 'required|min_length[6]|md5');
		$this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
		if ($this->form_validation->run())
                {   

                	$data = array(

                        'email' => $this->input->post('email'),

                        'password' => $this->input->post('pass')

                        );

                	$this->load->model('loginmodel');

                	$login_id = $this->loginmodel->log_usr($data);

                	if($login_id){

                		$this->session->set_userdata('user_id', $login_id);

                		redirect('admin_dashboard','refresh');

                	}	

                }

                else

                {

                        $this->load->view('login');

                }

	}

}