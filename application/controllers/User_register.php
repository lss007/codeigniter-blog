<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User_register extends CI_Controller {

	public function index()

	{

		$this->load->helper('form');

		$this->load->view('register');

		$this->load->helper('security');

	}

	public function usr_reg(){

		$this->load->library('form_validation');

		$this->form_validation->set_rules('firstname', 'First Name', 'required|alpha');

		$this->form_validation->set_rules('lname', 'Last Name', 'required|alpha');

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');

		$this->form_validation->set_rules('pass', 'Password', 'required|min_length[6]|md5');

		$this->form_validation->set_rules('cpass', 'Confirm Password', 'required|md5|matches[pass]');

		$this->form_validation->set_rules('phone', 'Phone', 'required|integer|exact_length[10]|is_unique[users.phone]');

		$this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");

		if ($this->form_validation->run())

                {

                	$data = array(

                        'firstname' => $this->input->post('firstname'),

                        'lname' => $this->input->post('lname'),

                        'email' => $this->input->post('email'),

                        'password' => $this->input->post('pass'),

                        'phone' => $this->input->post('phone')

                        );

                	$this->load->model('regmodel');

                	if(!$this->regmodel->reg_usr($data)){

                        redirect('user_login', 'refresh');

                    }

                }

                else

                {

                        $this->load->view('register');

                }

	}

}
