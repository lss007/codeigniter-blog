<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class All_posts extends CI_Model{

 	public function posts_data($limit, $offset){	
 	$uid = $this->session->userdata('user_id');
       // $qry = $this->db->order_by('id', 'DESC')->limit($limit, $offset)->get('articles');
	   
	  /*$this->db->select('*');
		$this->db->from('articles'); 
		$this->db->join('users', 'users.id=articles.user_id', 'left');*/
		
		
		$this->db->select('*');
		$this->db->select('users.firstname, users.lname');
		$this->db->from('users');
		$this->db->join('articles','articles.user_id = users.id','left');
		
		$this->db->order_by('articles.id', 'DESC'); 
		$this->db->limit($limit, $offset);        
		$qry = $this->db->get();
	   
	   
		//$qry = $this->db->order_by('id', 'DESC')->limit($limit, $offset)->join('users', 'articles.user_id = users.id', 'FULL OUTER')->get();
        if($qry->num_rows()){
          return $qry->result();
       }
       else{
       	return false;
       }       

     }
	 
	 	public function num_rows(){
		 $uid = $this->session->userdata('user_id');
			$qry = $this->db->order_by('id', 'DESC')->get('articles');
			if($qry->num_rows()){
			  return $qry->num_rows();
		   }
		   else{
			return false;
       }
		}

 }