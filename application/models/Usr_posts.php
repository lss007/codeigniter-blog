<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 class Usr_posts extends CI_Model{
 	public function articles_list($limit, $offset){	
 		$uid = $this->session->userdata('user_id');
        $qry = $this->db->where('user_id', $uid)->limit($limit, $offset)->get('articles');
       return $qry->result();
 	}
	public function cunt_list(){	
 		$uid = $this->session->userdata('user_id');
        $qry = $this->db->where('user_id', $uid)->get('articles');
       return $qry->num_rows();
 	}
	 public function show_edt_post($id){
	 $uid = $this->session->userdata('user_id');
	 $qry = $this->db->where('id', $id)->where('user_id', $uid)->get('articles');
     return $qry->result();	
	}
	 public function edt_post($updtdata){
	 $uid = $this->session->userdata('user_id');
	  $qry = $this->db->where('id', $updtdata['id'])->where('user_id', $uid)->set('title', $updtdata['title'])->set('image_url', $updtdata['image_url'])->set('msg_body', $updtdata['msg_body'])->set('post_date', $updtdata['post_date'])->update('articles');
	}
	public function delete_post($id){
	   $uid = $this->session->userdata('user_id');
	   $qry = $this->db->where('user_id', $uid)->where('id', $id)->delete('articles');
       return true;		
	}

 }