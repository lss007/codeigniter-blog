<?php include_once('admin_header.php'); 
	?>
  <div class="dash_bord log_usr">
  	<div class="container">
        <div class="enter_post" id="edtfom">
        <?php foreach ($dataarticles as $data) : ?>
        <?php echo form_open_multipart('admin_dashboard/post_upd', ['class'=>'fom_dash']); ?>        	
  			<h2>Update Post Here</h2>
			  <div class="form-group">
              <?php echo form_input(['type'=>'hidden', 'class'=>'form-control', 'name'=>'post_id', 'value'=> $data->id ]);  ?>
			    <label for="email">Title:</label>
			    <?php echo form_input(['type'=>'text', 'class'=>'form-control', 'name'=>'post_title', 'value'=> $data->title ]);  ?>
	            <?php echo form_error('post_title'); ?>
			  </div>
			  <div class="form-group">
			    <label for="exampleFormControlTextarea1">Description</label>			    
			    <?php echo form_textarea(['class'=>'form-control', 'name'=>'post_desc', 'rows'=>'5', 'value'=> $data->msg_body]);  ?>
			    <?php echo form_error('post_desc'); ?>
			  </div>
			  <div class="form-group">
			  	<label for="email">Add Image:</label>
			    <?php echo form_upload(['class'=>'form-control-file', 'name'=>'post_img', 'value'=>  $data->image_url]); ?>
			    <?php if(isset($upload_error)) echo $upload_error; ?>
			  </div>
			  <?php echo form_submit(['name'=>'upd_post', 'class'=>'btn btn-success sbtn', 'value'=>'Update']); ?>
	          <?php echo form_submit(['name'=>'usr_can', 'class'=>'btn btn-default sbtn', 'value'=>'Cancel']); ?>
			</form>  
			<?php endforeach;  ?>          
		</div>
		<div class="all_posts">
			<h2>All Posts</h2>			
		<table class="table table-striped table-responsive">
		    <thead>
		      <tr>
		      	<th>Sr.</th>
		        <th>Date</th>
		        <th>Title</th>
		        <th>Edit</th>
		        <th>Delete</th>
		      </tr>
		    </thead>
		    <tbody>
		    <?php if( count($articles) ): 
		    $i = 1;
		    ?>
                    <?php foreach ($articles as $article) : ?>
                    <?php $id = $article->id; ?>	
		    <tr>
		      	<td><?php echo $i;?></td>
		        <td><?php echo $article->post_date ?></td>
		        <td><?php echo $article->title ?></td>
		        <td><a href="<?php echo base_url('show_edit/index');?>?&id=<?php echo $id;?>" onclick="toggleOptions(this);" class="btn btn-primary" name="edit_post" id="edt_pst">Edit</a></td>
		        <td><a href="<?php echo base_url('admin_dashboard/post_dlte');?>?&id=<?php echo $id;?>" class="btn btn-danger" name="delete_post">Delete</a></td>
		       </tr>
		  <?php 
		   $i++;
		  endforeach;  ?>
              <?php else: ?>
                <tr>
                 	<td colspan="4" class="no_rec"><p>You have not entered any post yet.</p></td>
                 </tr>
              <?php endif;   ?>
               </tbody>
		  </table>
		</div>
        <div class="pag_num">
              <?php echo $this->pagination->create_links(); ?>
        </div>
  	</div>
  </div>
<?php include('admin_footer.php'); ?>