<?php include_once('main_header.php'); ?>
      <div class="blog_sec">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <!-- Blog Post -->
              <?php if( count($allposts) ): ?>
                <?php foreach ($allposts as $allpost) :?>                 
                  <?php  $id = $allpost->id; ?>
                  <div class="row">
                    <div class="col-sm-6">
                      <span style="background-image: url(<?php echo $allpost->image_url ; ?>);" class="imgPlaceholder"></span>
                       <!--<img class="card-img-top" src="" alt="Card image cap"> -->
                    </div>
                    <div class="col-sm-6">
                 <div class="card mb-4">                
                  <div class="card-body">
                    <h2 class="card-title"><?php echo $allpost->title ; ?></h2>
                    <p class="card-text"><?php $string = $allpost->msg_body; echo character_limiter($string, 200); ?></p>
                      <a href="<?php echo base_url('single_view');?>?&id=<?php echo $id;?>" class="btn btn-primary">Read More &rarr;</a>
                  </div>
                  <div class="card-footer text-muted">
                    Posted on <span class="pst_dte"><?php echo $allpost->post_date ; ?></span> by
                    <a href="#"><?php echo $allpost->firstname ." ". $allpost->lname ; ?></a>
                  </div>
               </div>
               </div>
            </div>
              <?php endforeach;  ?>
              <?php else: ?>
                 <div class="no_data_home">
                     <p>There is no any data by any user. Please signup and be a first user to post in this site :) </p>
                 </div>
              <?php endif;   ?>

              <!-- Pagination -->
              <?php echo $this->pagination->create_links(); ?>

            </div>
          </div>
        </div>
      </div>
  <?php include('main_footer.php'); ?>