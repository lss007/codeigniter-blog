<?php  include_once('main_header.php'); ?>
  <div class="log_usr">
  <div class="container">
  	<?php echo form_open('User_login/usr_log', ['class'=>'fom_reg']); ?>
  	  <h2>Login Here</h2>
	  <div class="form-group">
	    <label for="email">Email address:</label>
	    <?php echo form_input(['type'=>'email', 'class'=>'form-control', 'name'=>'email', 'value'=>set_value('email')]);  ?>
	    <?php echo form_error('email'); ?>
	  </div>
	  <div class="form-group">
	    <label for="pwd">Password:</label>
	    <?php echo form_input(['type'=>'password', 'class'=>'form-control', 'name'=>'pass', 'value'=>set_value('lname')]);  ?>
	    <?php echo form_error('pass'); ?>
	  </div>
	  <?php echo form_submit(['name'=>'usr_logg', 'class'=>'btn btn-success sbtn', 'value'=>'Login']); ?>
	  <?php echo form_reset(['name'=>'usr_can', 'class'=>'btn btn-default sbtn', 'value'=>'Cancel']); ?>
	</form>
	<div class="social_login">	
     <a href="<?php echo base_url('/example/web_login'); ?>">facebok.login</a>   
		<!--<div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>-->
	</div>
	<div id="status"></div>
	<div id="fb-root"></div>

	</div>
  </div>
<?php  include('main_footer.php'); ?>