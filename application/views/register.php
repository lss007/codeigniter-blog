<?php  include_once('main_header.php'); ?>
  <div class="log_usr">
  <div class="container">
  		<?php echo form_open('User_register/usr_reg', ['class'=>'fom_reg']); ?>
  	  <h2>Register Here</h2>
  	  <div class="form-group">
	    <label for="name">First Name:</label>
	    <?php echo form_input(['type'=>'text', 'class'=>'form-control', 'name'=>'firstname', 'value'=>set_value('firstname')]);  ?>
	    <?php echo form_error('firstname'); ?>
	  </div>
	  <div class="form-group">
	    <label for="name">Last Name:</label>
	    <?php echo form_input(['type'=>'text', 'class'=>'form-control', 'name'=>'lname', 'value'=>set_value('lname')]);  ?>
	    <?php echo form_error('lname'); ?>
	  </div>
	  <div class="form-group">
	    <label for="email">Email address:</label>
	    <?php echo form_input(['type'=>'email', 'class'=>'form-control', 'name'=>'email', 'value'=>set_value('email')]);  ?>
	    <?php echo form_error('email'); ?>
	  </div>
	  <div class="form-group">
	    <label for="pwd">Password:</label>
	    <?php echo form_input(['type'=>'password', 'class'=>'form-control', 'name'=>'pass', 'value'=>set_value('pass')]);  ?>
	    <?php echo form_error('pass'); ?>
	  </div>
	  <div class="form-group">
	    <label for="cpwd"> Confirm Password:</label>
	    <?php echo form_input(['type'=>'password', 'class'=>'form-control', 'name'=>'cpass', 'value'=>set_value('cpass')]);  ?>
	    <?php echo form_error('cpass'); ?>
	  </div>
	  <div class="form-group">
	    <label for="phone">Phone:</label>
	    <?php echo form_input(['type'=>'text', 'class'=>'form-control', 'name'=>'phone', 'value'=>set_value('phone')]);  ?>
	    <?php echo form_error('phone'); ?>
	  </div>
	  <?php echo form_submit(['name'=>'usr_reg', 'class'=>'btn btn-success sbtn', 'value'=>'Register']); ?>
	  <?php echo form_reset(['name'=>'usr_can', 'class'=>'btn btn-default sbtn', 'value'=>'Cancel']); ?>
	</form>
	</div>
  </div>
<?php  include_once('main_footer.php'); ?>