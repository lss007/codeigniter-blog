<?php include_once('main_header.php');?>
      <div class="blog_sec log_usr">
        <div class="container">
          <div class="row">
            <div class="col-md-12">            		    
	       <?php foreach ($sig_data as $sig_dat) :?>               
                <div class="card mb-4">                
                  <div class="card-body">
                  <img src="<?php echo $sig_dat->image_url ; ?>" class="sigle_img"/>
                    <h2 class="card-title"><?php echo $sig_dat->title ; ?></h2>
                    <p class="card-text"><?php echo $sig_dat->msg_body ; ?></p>
                  </div>
                  <div class="card-footer text-muted">
                    Posted on <span class="pst_dte"><?php echo $sig_dat->post_date ; ?></span> by
                    <a href="#">Start Bootstrap</a>
                  </div>
               </div>
               <?php endforeach;  ?>

              <!-- Pagination -->
              <ul class="pagination justify-content-center mb-4">
                <li class="page-item">
                  <a class="page-link" href="#">&larr; Older</a>
                </li>
                <li class="page-item disabled">
                  <a class="page-link" href="#">Newer &rarr;</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

  <?php include('main_footer.php'); ?>